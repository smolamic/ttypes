<?php namespace Addressbook;

use \ArrayAccess;

class Addressbook implements ArrayAccess
{
    const SCHEMA_VERSION = "0.1";
    const TYPE_VERSION = "Addressbook@0.1";
    const JSON_CONTENT_TYPE= "application/json; ttype=Addressbook@0.1";

    /** @var array */
    private $inner;

    public function __construct(array $inner)
    {
        $this->inner = $inner;
    }

    public function offsetExists($offset) : bool
    {
        return isset($this->inner[$offset]);
    }

    public function offsetGet($offset)
    {
        return $this->inner[$offset];
    }

    public function offsetSet($offset, $value)
    {
        $this->inner[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        unset($this->inner[$offset]);
    }
}