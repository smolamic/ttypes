<?php namespace Addressbook\PhoneNumberType;
require_once(__DIR__.'/../PhoneNumberType.php');

use Addressbook\PhoneNumberType;

class Home extends PhoneNumberType
{
    public function __construct()
    {
        parent::__construct("Home");
    }
}