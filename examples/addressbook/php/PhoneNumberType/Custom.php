<?php namespace Addressbook\PhoneNumberType;

use Addressbook\PhoneNumberType;

class Custom extends PhoneNumberType
{
    /** @var string */
    public $name;

    public function __construct(array $fields)
    {
        parent::__construct("Custom");
        $this->name = $fields['name'];
    }
}