<?php namespace Addressbook\{PhoneNumberType};

use Addressbook\{PhoneNumberType};

class  extends PhoneNumberType {
    /** @var string */
    public $name;

    public function __construct($fields)
    {
        $this->name = $fields['name'];
    }
}