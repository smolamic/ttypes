<?php namespace Addressbook;

class PhoneNumber {
    const SCHEMA_VERSION = "0.1";
    const TYPE_VERSION = "PhoneNumber@0.1";
    const JSON_CONTENT_TYPE= "application/json; ttype=PhoneNumber@0.1";

    /** @var PhoneNumberType */
    public $type;
    /** @var string */
    public $number;

    public function __construct(array $fields)
    {
        $this->type = $fields['type'];
        $this->number = $fields['number'];
    }

}