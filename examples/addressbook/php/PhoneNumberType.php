<?php namespace Addressbook;

abstract class PhoneNumberType {
    const SCHEMA_VERSION = "0.1";
    const TYPE_VERSION = "PhoneNumberType@0.1";
    const JSON_CONTENT_TYPE= "application/json; ttype=PhoneNumberType@0.1";

    /** @var string */
    public $__tag;

    protected function __construct(string $tag)
    {
        $this->__tag = $tag;
    }
}