<?php namespace Addressbook;

class Contact {
    const SCHEMA_VERSION = "0.1";
    const TYPE_VERSION = "Contact@0.1";
    const JSON_CONTENT_TYPE= "application/json; ttype=Contact@0.1";

    /** @var string */
    public $name;
    /** @var array */
    public $phone;

    public function __construct(array $fields)
    {
        $this->name = $fields['name'];
        $this->phone = $fields['phone'];
    }

}