<?php namespace TTypes;
/**
 * @package TTypes
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
require(__DIR__.'/../vendor/autoload.php');

use TTypes\Types\Schema;
use \SimpleXMLElement;

$config = Config::fromXML(new SimpleXMLElement(file_get_contents($argv[1])));
$schema = Schema::fromXML(new SimpleXMLElement(file_get_contents($argv[2])));

$outfiles = [];
foreach ($config->backends() as $backend)
    $outfiles = array_merge($outfiles, $backend->buildTypes($schema));

foreach ($outfiles as $outfile)
    $outfile->write();
