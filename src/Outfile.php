<?php namespace TTypes;
/**
 * @package TTypes
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
// @codeCoverageIgnoreStart
require(__DIR__.'/../vendor/autoload.php');
// @codeCoverageIgnoreEnd

class Outfile {
    /** @var string */
    private $filename;
    /** @var string */
    private $content;

    public function __construct(string $filename, string $content)
    {
        $this->filename = $filename;
        $this->content = $content;
    }

    public function write()
    {
        echo "write $this->filename\n";
        $dir = dirname($this->filename);
        if (!is_dir($dir)) {
            mkdir($dir, 0755, true);
        }
        file_put_contents($this->filename, $this->content);
    }

    public function filename() : string
    {
        return $this->filename;
    }

    public function content() : string
    {
        return $this->content;
    }
}
