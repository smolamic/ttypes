<?php namespace TTypes\PHP;
/**
 * @package TTypes
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
// @codeCoverageIgnoreStart
require(__DIR__.'/../../vendor/autoload.php');
// @codeCoverageIgnoreEnd

use TTypes\Types\Schema;
use TTypes\Types\Struct;
use TTypes\Types\Union;
use TTypes\Types\Alias;
use TTypes\Types\Variant;
use TTypes\Types\Field;
use TTypes\Outfile;
use \SimpleXMLElement;

class Backend {
    /** @var string */
    private $dir;
    /** @var string */
    private $namespace;

    public function __construct(string $dir, string $namespace)
    {
        $this->dir = $dir;
        $this->namespace = $namespace;
    }

    public static function fromXML(SimpleXMLElement $def) : Self
    {
        return new Self($def['dir'], $def['namespace']);
    }

    public function dir() : string
    {
        return $this->dir;
    }

    public function namespace() : string
    {
        return $this->namespace;
    }

    public function buildTypes(Schema $schema) : array
    {
        $outfiles = [];
        foreach ($schema->types() as $type) {
            if ($type instanceof Struct) {
                $outfiles[] = $this->buildStruct($type, $schema->version());
                continue;
            }

            if ($type instanceof Union) {
                $outfiles = array_merge($outfiles, $this->buildUnion($type, $schema->version()));
                continue;
            }

            if ($type instanceof Alias) {
                $outfiles[] = $this->buildAlias($type, $schema->version());
                continue;
            }
        }

        return $outfiles;
    }

    private function buildType(string $type) : array
    {
        echo "buildType $type\n";
        $matches;
        $optional = false;
        if (preg_match("/^\(([A-Za-z0-9\[\]\{\}]+)\)$/", $type, $matches)) {
            echo "optional\n";
            // an optional type in php is just its inner type but could be null
            $type = $matches[1];
            $optional = true;
        }
        if (preg_match("/^\[([A-Za-z0-9]+)\]$/", $type)) {
            // a list is a php array
            return ["array", $optional];
        }
        if (preg_match("/^\{[A-Za-z0-9]+\}$/", $type)) {
            // a dict is a php array
            return ["array", $optional];
        }
        return [$type, $optional];
    }

    private function buildField(Field $field) : array
    {
        $name = $field->name();
        list($type, $optional) = $this->buildType($field->type());

        $member = $optional ? <<<PHP
    /** @var $type|null */
    public \$$name;
PHP : <<<PHP
    /** @var $type */
    public \$$name;
PHP;

        $setter = $optional ? <<<PHP
        if (array_key_exists('$name', \$fields))
            \$this->$name = \$fields['$name'];
PHP : <<<PHP
        \$this->$name = \$fields['$name'];
PHP;

        return [$member, $setter];
    }

    public function buildStruct(Struct $struct, string $version) : Outfile
    {
        $members = [];
        $setters = [];

        foreach ($struct->fields() as $field) {
            list($member, $setter) = $this->buildField($field);
            $members[] = $member;
            $setters[] = $setter;
        }

        $members = implode("\n", $members);
        $setters = implode("\n", $setters);

        $body = <<<PHP
<?php namespace {$this->namespace};

class {$struct->name()} {
    const SCHEMA_VERSION = "$version";
    const TYPE_VERSION = "{$struct->name()}@$version";
    const JSON_CONTENT_TYPE= "application/json; ttype={$struct->name()}@$version";

$members

    public function __construct(array \$fields)
    {
$setters
    }

    public static function deserialize(array \$arr) : Self
    {
        return new Self(\$arr);
    }
}
PHP;
        return new Outfile("{$this->dir}/{$struct->name()}.php", $body);
    }

    public function buildUnion(Union $union, string $version) : array
    {
        $outfiles = [$this->buildUnionBase($union, $version)];

        foreach ($union->variants() as $variant)
            $outfiles[] = $this->buildVariant($union->name(), $variant);

        return $outfiles;
    }

    private function buildUnionBase(Union $union, string $version) : Outfile
    {
        $body = <<<PHP
<?php namespace {$this->namespace};

abstract class {$union->name()} {
    const SCHEMA_VERSION = "$version";
    const TYPE_VERSION = "{$union->name()}@$version";
    const JSON_CONTENT_TYPE= "application/json; ttype={$union->name()}@$version";

    /** @var string */
    public \$__tag;

    protected function __construct(string \$tag)
    {
        \$this->__tag = \$tag;
    }
}
PHP;

        return new Outfile("{$this->dir}/{$union->name()}.php", $body);
    }

    private function buildVariant(string $unionName, Variant $variant) : Outfile
    {
        $body;
        if (empty($variant->fields())) {
            $body = <<<PHP
<?php namespace {$this->namespace}\\{$unionName};
require_once(__DIR__.'/../{$unionName}.php');

use {$this->namespace}\\{$unionName};
use \JsonSerializable;

class {$variant->name()} extends {$unionName} implements JsonSerializable
{
    public function __construct()
    {
        parent::__construct("{$variant->name()}");
    }

    public function jsonSerialize()
    {
        return \$this->__tag;
    }
}
PHP;
        } else {

            $members = [];
            $setters = [];

            foreach ($variant->fields() as $field) {
                list($member, $setter) = $this->buildField($field);
                $members[] = $member;
                $setters[] = $setter;
            }

            $members = implode("", $members);
            $setters = implode("", $setters);

            $body = <<<PHP
<?php namespace {$this->namespace}\\{$unionName};

use {$this->namespace}\\{$unionName};

class {$variant->name()} extends {$unionName}
{
$members

    public function __construct(array \$fields)
    {
        parent::__construct("{$variant->name()}");
$setters
    }
}
PHP;
        }

        return new Outfile(
            "{$this->dir}/{$unionName}/{$variant->name()}.php",
            $body
        );
    }

    public function buildAlias(Alias $alias, string $version) : Outfile
    {
        list($type, $optional) = $this->buildType($alias->type());

        $orNull = $optional ? '|null' : '';
        $isNull = $optional ? ' = null' : '';

        $consts = <<<PHP
    const SCHEMA_VERSION = "$version";
    const TYPE_VERSION = "{$alias->name()}@$version";
    const JSON_CONTENT_TYPE= "application/json; ttype={$alias->name()}@$version";
PHP;

        $class;
        switch ($type) {
        case 'string':
            $class = <<<PHP
use \JsonSerializable;

class {$alias->name()} implements JsonSerializable
{
$consts

    /** @var $type$orNull */
    public \$value;

    public function __construct($type \$value$isNull)
    {
        \$this->value = \$value;
    }

    public function __toString() : string
    {
        return \$this->value;
    }

    public function jsonSerialize()
    {
        return \$this->value;
    }
}
PHP;
            break;

        case 'array':
            $class = <<<PHP
use \ArrayAccess;
use \JsonSerializable;

class {$alias->name()} implements ArrayAccess, JsonSerializable
{
$consts

    /** @var $type$orNull */
    private \$inner;

    public function __construct($type \$inner$isNull)
    {
        \$this->inner = \$inner;
    }

    public function offsetExists(\$offset) : bool
    {
        return isset(\$this->inner[\$offset]);
    }

    public function offsetGet(\$offset)
    {
        return \$this->inner[\$offset];
    }

    public function offsetSet(\$offset, \$value)
    {
        \$this->inner[\$offset] = \$value;
    }

    public function offsetUnset(\$offset)
    {
        unset(\$this->inner[\$offset]);
    }

    public function jsonSerialize()
    {
        return \$this->inner;
    }
}
PHP;
            break;

        case 'int':
        case 'float':
        case 'bool':
            $class = <<<PHP
use \JsonSerializable;

class {$alias->name()} implements JsonSerializable
{
$consts
    /** @var $type$orNull */
    public \$value;

    public function __construct($type \$value$isNull)
    {
        \$this->value = \$value;
    }

    public function jsonSerialize()
    {
        return \$this->value;
    }
}
PHP;
            break;

        default:
            $class = <<<PHP
use \JsonSerializable;

class {$alias->name()} implements JsonSerializable
{
$consts
    /** @var $type$orNull */
    private \$inner;

    public function __construct($type \$inner$isNull)
    {
        \$this->inner = \$inner;
    }

    public function jsonSerialize()
    {
        return \$this->inner;
    }

    public function __get(string \$name)
    {
        return \$this->inner->\$name;
    }

    public function __set(string \$name, \$value)
    {
        \$this->inner->\$name = \$value;
    }

    public function __isset(string \$name) : bool
    {
        return isset(\$this->inner->\$name);
    }

    public function __unset(string \$name)
    {
        unset(\$this->inner->\$name);
    }
}
PHP;
        }


        $body = <<<PHP
<?php namespace {$this->namespace};

$class
PHP;

        return new Outfile("{$this->dir}/{$alias->name()}.php", $body);
    }
}
