<?php namespace TTypes;
/**
 * @package TTypes
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
// @codeCoverageIgnoreStart
require(__DIR__.'/../vendor/autoload.php');
// @codeCoverageIgnoreEnd


use TTypes\PHP\Backend as PHPBackend;
use \SimpleXMLElement;
use \Exception;

class Config {
    /** @var array */
    private $backends;

    public function __construct(array $backends)
    {
        $this->backends = $backends;
    }

    public static function fromXML(SimpleXMLElement $def) : Self
    {
        $backends = [];
        foreach ($def->output as $output) {
            $lang = (string) $output['lang'];
            echo "processing output $lang\n";
            switch ($lang) {
            case 'php':
                $backends[$lang] = PHPBackend::fromXML($output);
                break;
            default:
                throw new Exception("Unrecognized output language \"{$lang}\"");
            }
        }

        return new Self($backends);
    }

    public function backend(string $lang)
    {
        return $this->backends[$lang];
    }

    public function backends() : array
    {
        return $this->backends;
    }
}
