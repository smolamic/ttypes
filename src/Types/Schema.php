<?php namespace TTypes\Types;
/**
 * @package TTypes
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
// @codeCoverageIgnoreStart
require(__DIR__.'/../../vendor/autoload.php');
// @codeCoverageIgnoreEnd

use \SimpleXMLElement;
use \Exception;

class Schema {
    /** @var Type[] */
    private $types;
    /** @var string */
    private $version;

    public function __construct(string $version, array $types)
    {
        $this->types = $types;
        $this->version = $version;
    }

    public static function fromXML(SimpleXMLElement $def) : Self
    {
        $version = (string) $def['version'];

        $types = [];
        foreach ($def->children() as $child) {
            $type;
            switch ($child->getName()) {
            case 'alias':
                $type = Alias::fromXML($child);
                break;
            case 'struct':
                $type = Struct::fromXML($child);
                break;
            case 'union':
                $type = Union::fromXML($child);
                break;
            default:
                throw new Exception("Unsupported Type definition: {$child->getName()}");
            }
            $types[$type->name()] = $type;
        }

        return new Self($version, $types);
    }

    public function types() : array
    {
        return $this->types;
    }

    public function type(string $name) : Type
    {
        return $this->types[$name];
    }

    public function version() : string
    {
        return $this->version;
    }
}
