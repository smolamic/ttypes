<?php namespace TTypes\Types;
/**
 * @package TTypes
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
// @codeCoverageIgnoreStart
require(PHP_ROOT.'vendor/autoload.php');
// @codeCoverageIgnoreEnd

class Basic {
    /** @var string */
    private $name;
    /** @var string[] */
    private $args;

    public function __construct(string $name, array $args)
    {
        $this->name = $name;
        $this->args = $args;
    }
}
