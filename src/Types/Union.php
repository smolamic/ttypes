<?php namespace TTypes\Types;
/**
 * @package TTypes
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
// @codeCoverageIgnoreStart
require(__DIR__.'/../../vendor/autoload.php');
// @codeCoverageIgnoreEnd

use \SimpleXMLElement;

class Union extends Type {
    /** @var Variant[] */
    private $variants;

    public function __construct(string $name, array $variants)
    {
        parent::__construct($name);
        $this->variants = $variants;
    }

    public static function fromXML(SimpleXMLElement $def) : Self
    {
        $name = $def['name'];

        $variants = [];
        foreach ($def->variant as $variant)
            $variants[] = Variant::fromXML($variant);

        return new Self($name, $variants);
    }

    public function variants() : array
    {
        return $this->variants;
    }
}
