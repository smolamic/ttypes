<?php namespace TTypes\Types;
/**
 * @package TTypes
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
// @codeCoverageIgnoreStart
require(__DIR__.'/../../vendor/autoload.php');
// @codeCoverageIgnoreEnd

use \SimpleXMLElement;

class Variant {
    /** @var string */
    private $name;
    /** @var Field[] */
    private $fields;

    public function __construct(string $name, array $fields)
    {
        $this->name = $name;
        $this->fields = $fields;
    }

    public static function fromXML(SimpleXMLElement $def) : Self
    {
        $name = (string) $def['name'];
        $fields = [];
        foreach ($def->field as $field)
            $fields[] = Field::fromXML($field);

        return new Self($name, $fields);
    }

    public function name() : string
    {
        return $this->name;
    }

    public function fields() : array
    {
        return $this->fields;
    }
}
