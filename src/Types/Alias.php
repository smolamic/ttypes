<?php namespace TTypes\Types;
/**
 * @package TTypes
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
// @codeCoverageIgnoreStart
require(__DIR__.'/../../vendor/autoload.php');
// @codeCoverageIgnoreEnd

use \SimpleXMLElement;

class Alias extends Type {
    /** @var string */
    private $type;

    public function __construct(string $name, string $type)
    {
        parent::__construct($name);
        $this->type = $type;
    }

    public static function fromXML(SimpleXMLElement $def) : Self
    {
        $name = $def['name'];
        $type = $def['type'];

        return new Self($name, $type);
    }

    public function type() : string
    {
        return $this->type;
    }
}
