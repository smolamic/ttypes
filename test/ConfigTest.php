<?php namespace TTypes\Test;
/**
 * @package TTypes
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
require(__DIR__.'/../vendor/autoload.php');

use TTypes\Config;
use TTypes\PHP\Backend as PHPBackend;
use PHPUnit\Framework\TestCase;
use \SimpleXMLElement;

class ConfigTest extends TestCase
{
    public function testSetAndGet()
    {
        $config = new Config([
            'php' => new PHPBackend('Api', 'Test\Api'),
        ]);

        $phpBackend = $config->backend('php');

        $this->assertSame('Api', $phpBackend->dir());
        $this->assertSame('Test\Api', $phpBackend->namespace());
    }

    public function testFromXml()
    {
        $config = Config::fromXML(new SimpleXMLElement(<<<XML
<?xml version="1.0"?>
<config>
    <output lang="php" dir="Api" namespace="Test\Api"></output>
</config>
XML
        ));

        $phpBackend = $config->backend('php');

        $this->assertSame('Api', $phpBackend->dir());
        $this->assertSame('Test\Api', $phpBackend->namespace());
    }
}
