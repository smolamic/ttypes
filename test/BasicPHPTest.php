<?php namespace TTypes\Test;
/**
 * @package TTypes
 * @author Michel Smola
 * @license https://www.gnu.org/licenses/gpl-3.0.en.html GNU General Public License, version 3
 */
require(__DIR__.'/../vendor/autoload.php');

use TTypes\Types\Struct;
use TTypes\Types\Field;
use TTypes\Types\Union;
use TTypes\Types\Variant;
use TTypes\Types\Alias;
use TTypes\Types\Schema;
use TTypes\PHP\Backend as PHPBackend;
use PHPUnit\Framework\TestCase;

class BasicPHPTest extends TestCase
{
    const TMPDIR = __DIR__.'/tmp';
    /** @var PHPBackend */
    private $phpBackend;

    protected function setUp() : void
    {
        mkdir(Self::TMPDIR);
        $this->phpBackend = new PHPBackend(Self::TMPDIR, 'Test');
    }

    protected function tearDown() : void
    {
        array_map('unlink', glob(Self::TMPDIR.'/*.*'));
        array_map('unlink', glob(Self::TMPDIR.'/*/*.*'));
        array_map('rmdir', glob(Self::TMPDIR.'/*'));
        rmdir(Self::TMPDIR);
    }

    public function testStructToPHP()
    {
        $struct = new Struct("StructuralType", [
            new Field("string", "string"),
            new Field("int", "int"),
            new Field("float", "float"),
            new Field("bool", "bool"),
            new Field("list", "[string]"),
            new Field("dict", "{string}"),
            new Field("optional", "(string)"),
        ]);


        $outfile = $this->phpBackend->buildStruct($struct, "0.0.1");
        $outfile->write();
        require_once($outfile->filename());

        $fields = [
            'string' => 'abc',
            'int' => 92,
            'float' => 3.14,
            'bool' => true,
            'list' => ['def', 'ghi', 'jkl'],
            'dict' => ['a' => 'bcd', 'e' => 'fgh', 'i' => 'jkl'],
        ];

        $instance = new \Test\StructuralType($fields);

        $this->assertSame('abc', $instance->string);
        $this->assertSame(92, $instance->int);
        $this->assertSame('3.14', (string) $instance->float);
        $this->assertTrue($instance->bool);
        $this->assertEquals(['def', 'ghi', 'jkl'], $instance->list);
        $this->assertSame('bcd', $instance->dict['a']);
        $this->assertSame('fgh', $instance->dict['e']);
        $this->assertSame('jkl', $instance->dict['i']);
        $this->assertNull($instance->optional);

        $fields['optional'] = null;
        $this->assertSame(json_encode($fields), json_encode($instance));

        $this->assertSame('0.0.1', \Test\StructuralType::SCHEMA_VERSION);
        $this->assertSame('StructuralType@0.0.1', \Test\StructuralType::TYPE_VERSION);
        $this->assertSame('application/json; ttype=StructuralType@0.0.1', \Test\StructuralType::JSON_CONTENT_TYPE);

        $instance = \Test\StructuralType::deserialize($fields);
        $this->assertSame('abc', $instance->string);
        $this->assertSame(92, $instance->int);
        $this->assertSame('3.14', (string) $instance->float);
        $this->assertTrue($instance->bool);
        $this->assertEquals(['def', 'ghi', 'jkl'], $instance->list);
        $this->assertSame('bcd', $instance->dict['a']);
        $this->assertSame('fgh', $instance->dict['e']);
        $this->assertSame('jkl', $instance->dict['i']);
        $this->assertNull($instance->optional);
    }

    public function testUnionToPHP()
    {
        $union = new Union('UnionType', [
            new Variant('PlainVariant', []),
            new Variant('VariantWithField', [
                new Field('string', 'string'),
            ]),
        ]);

        $outfiles = $this->phpBackend->buildUnion($union, "0.0.1");
        foreach ($outfiles as $outfile) {
            $outfile->write();
            require_once($outfile->filename());
        }

        $this->assertSame('0.0.1', \Test\UnionType::SCHEMA_VERSION);
        $this->assertSame('UnionType@0.0.1', \Test\UnionType::TYPE_VERSION);
        $this->assertSame('application/json; ttype=UnionType@0.0.1', \Test\UnionType::JSON_CONTENT_TYPE);

        $plainVariant = new \Test\UnionType\PlainVariant();
        $this->assertSame('PlainVariant', $plainVariant->__tag);
        $this->assertSame(json_encode('PlainVariant'), json_encode($plainVariant));
        $this->assertSame('0.0.1', \Test\UnionType\PlainVariant::SCHEMA_VERSION);
        $this->assertSame('UnionType@0.0.1', \Test\UnionType\PlainVariant::TYPE_VERSION);
        $this->assertSame('application/json; ttype=UnionType@0.0.1', \Test\UnionType\PlainVariant::JSON_CONTENT_TYPE);

        $variantWithField = new \Test\UnionType\VariantWithField([
            'string' => 'abc',
        ]);
        $this->assertSame('VariantWithField', $variantWithField->__tag);
        $this->assertSame(
            json_encode(['string' => 'abc', '__tag' => 'VariantWithField']),
            json_encode($variantWithField)
        );
    }

    public function testAliasToPHP()
    {
        $struct = new Struct('Struct', [
            new Field('string', 'string'),
        ]);

        $union = new Union('Union', [
            new Variant('PlainVariant', []),
            new Variant('StringVariant', [
                new Field('string', 'string'),
            ]),
        ]);

        $stringAlias = new Alias('StringAlias', 'string');
        $intAlias = new Alias('IntAlias', 'int');
        $floatAlias = new Alias('FloatAlias', 'float');
        $boolAlias = new Alias('BoolAlias', 'bool');
        $optStringAlias = new Alias('OptStringAlias', '(string)');
        $optIntAlias = new Alias('OptIntAlias', '(int)');
        $optFloatAlias = new Alias('OptFloatAlias', '(float)');
        $optBoolAlias = new Alias('OptBoolAlias', '(bool)');
        $listAlias = new Alias('ListAlias', '[string]');
        $dictAlias = new Alias('DictAlias', '{string}');
        $listListAlias = new Alias('ListListAlias', '[ListAlias]');
        $structAlias = new Alias('StructAlias', 'Struct');
        $unionAlias = new Alias('UnionAlias', 'Union');

        $schema = new Schema("0.0.1", [
            $struct,
            $union,
            $stringAlias,
            $intAlias,
            $floatAlias,
            $boolAlias,
            $optStringAlias,
            $optIntAlias,
            $optFloatAlias,
            $optBoolAlias,
            $listAlias,
            $dictAlias,
            $listListAlias,
            $structAlias,
            $unionAlias,
        ]);

        $outfiles = $this->phpBackend->buildTypes($schema);
        foreach ($outfiles as $outfile) {
            $outfile->write();
            require_once($outfile->filename());
            //echo "\n\n{$outfile->filename()}\n{$outfile->content()}\n";
        }

        $inst = new \Test\StringAlias('abc');
        $this->assertSame('abc', (string) $inst);
        $this->assertSame('abc', $inst->value);
        $this->assertSame('0.0.1', \Test\StringAlias::SCHEMA_VERSION);
        $this->assertSame('StringAlias@0.0.1', \Test\StringAlias::TYPE_VERSION);
        $this->assertSame('application/json; ttype=StringAlias@0.0.1', \Test\StringAlias::JSON_CONTENT_TYPE);
        $this->assertSame(json_encode('abc'), json_encode($inst));

        $inst = new \Test\IntAlias(15);
        $this->assertSame(15, $inst->value);
        $this->assertSame(json_encode(15), json_encode($inst));

        $inst = new \Test\FloatAlias(3.14);
        $this->assertSame("3.14", (string) $inst->value);
        $this->assertSame(json_encode(3.14), json_encode($inst));

        $inst = new \Test\BoolAlias(true);
        $this->assertTrue($inst->value);
        $this->assertSame(json_encode(true), json_encode($inst));

        $inst = new \Test\OptStringAlias('abc');
        $this->assertSame('abc', (string) $inst);
        $this->assertSame('abc', $inst->value);
        $this->assertSame(json_encode('abc'), json_encode($inst));

        $inst = new \Test\OptStringAlias(null);
        $this->assertNull($inst->value);
        $this->assertSame(json_encode(null), json_encode($inst));

        $inst = new \Test\OptIntAlias(15);
        $this->assertSame(15, $inst->value);
        $this->assertSame(json_encode(15), json_encode($inst));

        $inst = new \Test\OptIntAlias(null);
        $this->assertNull($inst->value);
        $this->assertSame(json_encode(null), json_encode($inst));

        $inst = new \Test\OptFloatAlias(3.14);
        $this->assertSame("3.14", (string) $inst->value);
        $this->assertSame(json_encode(3.14), json_encode($inst));

        $inst = new \Test\OptFloatAlias(null);
        $this->assertNull($inst->value);
        $this->assertSame(json_encode(null), json_encode($inst));

        $inst = new \Test\OptBoolAlias(true);
        $this->assertTrue($inst->value);
        $this->assertSame(json_encode(true), json_encode($inst));

        $inst = new \Test\OptBoolAlias(null);
        $this->assertNull($inst->value);
        $this->assertSame(json_encode(null), json_encode($inst));

        $value = ['abc', 'def', 'ghi'];
        $inst = new \Test\ListAlias($value);
        $this->assertSame('abc', $inst[0]);
        $this->assertSame('def', $inst[1]);
        $this->assertSame('ghi', $inst[2]);
        $this->assertSame(json_encode($value), json_encode($inst));

        $value = ['a' => 'abc', 'b' => 'def', 'c' => 'ghi'];
        $inst = new \Test\DictAlias($value);
        $this->assertSame('abc', $inst['a']);
        $this->assertSame('def', $inst['b']);
        $this->assertSame('ghi', $inst['c']);
        $this->assertSame(json_encode($value), json_encode($inst));

        $value = [['abc', 'def'], ['ghi', 'jkl']];
        $inst = new \Test\ListListAlias($value);
        $this->assertSame('abc', $inst[0][0]);
        $this->assertSame('def', $inst[0][1]);
        $this->assertSame('ghi', $inst[1][0]);
        $this->assertSame('jkl', $inst[1][1]);
        $this->assertSame(json_encode($value), json_encode($inst));

        $inst = new \Test\StructAlias(new \Test\Struct([
            'string' => 'abc',
        ]));
        $this->assertSame('abc', $inst->string);
        $this->assertSame(json_encode(['string' => 'abc']), json_encode($inst));

        $inst = new \Test\UnionAlias(new \Test\Union\StringVariant(['string' => 'abc']));
        $this->assertSame('abc', $inst->string);
        $this->assertSame(
            json_encode(['string' => 'abc', '__tag' => 'StringVariant']),
            json_encode($inst)
        );

        $inst = new \Test\UnionAlias(new \Test\Union\PlainVariant());
        $this->assertSame(json_encode('PlainVariant'), json_encode($inst));
    }
}
